package assignments.assignment3;

class Dosen extends ElemenFasilkom {

    private MataKuliah mataKuliah;

    public Dosen(String nama) {
        super(nama, "Dosen");
    }

    public MataKuliah getMataKuliah() {
        return this.mataKuliah;
    }

    public void mengajarMataKuliah(MataKuliah mataKuliah) {
        if (this.mataKuliah != null) {
            System.out.printf("[DITOLAK] %s sudah mengajar mata kuliah %s\n", this, this.mataKuliah);
        } else if (mataKuliah.getDosen() != null) {
            System.out.printf("[DITOLAK] %s sudah memiliki dosen pengajar\n", mataKuliah);
        } else {
            this.mataKuliah = mataKuliah;
            mataKuliah.addDosen(this);
            System.out.printf("%s mengajar mata kuliah %s\n", this, mataKuliah);
        }
    }

    public void dropMataKuliah() {
        if (this.mataKuliah == null) {
            System.out.printf("[DITOLAK] %s sedang tidak mengajar mata kuliah apapun\n", this);
        } else {
            System.out.printf("%s berhenti mengajar %s\n", this, this.mataKuliah);
            this.mataKuliah.dropDosen();
            this.mataKuliah = null;
        }
    }

    @Override
    public void menyapa(ElemenFasilkom elemenFasilkom) {
        if (this.telahDisapa(elemenFasilkom) == -1) {
            System.out.printf("%s menyapa dengan %s\n", this, elemenFasilkom);

            // Menambah jumlah dan daftar elemen yang telah disapa
            this.setTelahMenyapa(elemenFasilkom);
            elemenFasilkom.setTelahMenyapa(this);

            // Nilai Friendship bertambah 2 jika Dosen dan Mahasiswa yang terhubung dalam mata kuliah yang sama saling menyapa
            if (elemenFasilkom.getTipe().equals("Mahasiswa") && this.mataKuliah != null) {
                if (this.mataKuliah.getMahasiswa((Mahasiswa) elemenFasilkom) != null) {
                    this.setFriendship(2);
                    elemenFasilkom.setFriendship(2);
                }
            }
        } else {
            System.out.printf("[DITOLAK] %s telah menyapa %s hari ini\n", this, elemenFasilkom);
        }
    }
}
package assignments.assignment3;

abstract class ElemenFasilkom {

    private String tipe;
    private String nama;
    private int friendship;
    private int jumlahMenyapa;
    private ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];

    public ElemenFasilkom(String nama, String tipe) {
        this.nama = nama;
        this.tipe = tipe;
    }

    public String getNama() {
        return this.nama;
    }

    public String getTipe() {
        return this.tipe;
    }

    public int getFriendship() {
        return this.friendship;
    }

    public int getJumlahMenyapa() {
        return this.jumlahMenyapa;
    }

    public void setFriendship(int friendship) {
        this.friendship += friendship;
        if (this.friendship>100) {
            this.friendship=100;
        } else if (this.friendship<0) {
            this.friendship=0;
        }
    }

    public void setTelahMenyapa(ElemenFasilkom elemenFasilkom) {
        this.telahMenyapa[this.jumlahMenyapa++] = elemenFasilkom;
    }

    public int telahDisapa(ElemenFasilkom elemenFasilkom) {
        for (int i=0; i<telahMenyapa.length; i++) {
            if (telahMenyapa[i] == elemenFasilkom) return i;
        }
        return -1;
    }

    public abstract void menyapa(ElemenFasilkom elemenFasilkom);

    public void resetMenyapa() {
        this.jumlahMenyapa = 0;
        this.telahMenyapa = new ElemenFasilkom[100];
    }

    public void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        Makanan makanan = ((ElemenKantin) penjual).getMakanan(namaMakanan);

        if (makanan != null) {
            System.out.printf("%s berhasil membeli %s seharga %d\n", pembeli, makanan, makanan.getHarga());
            pembeli.setFriendship(1);
            penjual.setFriendship(1);
        } else {
            System.out.printf("[DITOLAK] %s tidak menjual %s\n", penjual, namaMakanan);
        }
    }

    public String toString() {
        return this.nama;
    }
}
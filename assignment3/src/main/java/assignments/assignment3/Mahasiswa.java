package assignments.assignment3;

class Mahasiswa extends ElemenFasilkom {

    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    private int jumlahMataKuliah;
    private long npm;
    private String tanggalLahir;
    private String jurusan;

    public Mahasiswa(String nama, long npm) {
        super(nama, "Mahasiswa");
        this.npm = npm;
        this.tanggalLahir = extractTanggalLahir(npm);
        this.jurusan = extractJurusan(npm);
    }

    public String getTanggalLahir() {
        return this.tanggalLahir;
    }

    public String getJurusan() {
        return this.jurusan;
    }

    public int getJumlahMataKuliah() {
        return this.jumlahMataKuliah;
    }

    public MataKuliah[] getDaftarMataKuliah() {
        return this.daftarMataKuliah;
    }

    public int getIndeksMataKuliah(MataKuliah mataKuliah) {
        for (int i=0; i<this.daftarMataKuliah.length; i++) {
            if (mataKuliah == this.daftarMataKuliah[i]) return i;
        }
        return -1;
    }

    public void addMatkul(MataKuliah mataKuliah) {
        int indeksMataKuliah = getIndeksMataKuliah(mataKuliah);

        if (indeksMataKuliah != -1) {
            System.out.printf("[DITOLAK] %s telah diambil sebelumnya\n", mataKuliah);
        } else if (mataKuliah.getJumlahMahasiswa() == mataKuliah.getKapasitas()) {
            System.out.printf("[DITOLAK] %s telah penuh kapasitasnya\n", mataKuliah);
        } else {
            System.out.printf("%s berhasil menambahkan mata kuliah %s\n", this, mataKuliah);
            for (int i=0; i<this.daftarMataKuliah.length; i++){
                if (this.daftarMataKuliah[i] == null){
                    this.daftarMataKuliah[i] = mataKuliah;
                    mataKuliah.addMahasiswa(this);
                    this.jumlahMataKuliah++;
                    break;
                }
            }
        }
    }

    public void dropMatkul(MataKuliah mataKuliah) {
        int indeksMataKuliah = getIndeksMataKuliah(mataKuliah);

        if (indeksMataKuliah == -1) {
            System.out.printf("[DITOLAK] %s belum pernah diambil\n", mataKuliah);
        } else {
            this.daftarMataKuliah[indeksMataKuliah] = null;
            mataKuliah.dropMahasiswa(this);
            this.jumlahMataKuliah--;
            System.out.printf("%s berhasil drop mata kuliah %s\n", this, mataKuliah);
        }
    }

    public String extractTanggalLahir(long npm) {
        String npmStr = Long.toString(npm);
        int tanggal = Integer.parseInt(npmStr.substring(4,6));
        int bulan = Integer.parseInt(npmStr.substring(6,8));
        int tahun = Integer.parseInt(npmStr.substring(8,12));
        return String.format("%d-%d-%d", tanggal, bulan, tahun);
    }

    public String extractJurusan(long npm) {
        String npmStr = Long.toString(npm);
        return npmStr.substring(2,4).equals("01") ? "Ilmu Komputer" : "Sistem Informasi";
    }

    @Override
    public void menyapa(ElemenFasilkom elemenFasilkom) {
        if (this.telahDisapa(elemenFasilkom) == -1) {
            System.out.printf("%s menyapa dengan %s\n", this, elemenFasilkom);

            // Menambah jumlah dan daftar elemen yang telah disapa
            this.setTelahMenyapa(elemenFasilkom);
            elemenFasilkom.setTelahMenyapa(this);

            // Nilai Friendship bertambah 2 jika Mahasiswa dan Dosen yang terhubung dalam mata kuliah yang sama saling menyapa
            if (elemenFasilkom.getTipe().equals("Dosen") && ((Dosen) elemenFasilkom).getMataKuliah() != null) {
                if (((Dosen) elemenFasilkom).getMataKuliah().getMahasiswa(this) != null) {
                    this.setFriendship(2);
                    elemenFasilkom.setFriendship(2);
                }
            }
        } else {
            System.out.printf("[DITOLAK] %s telah menyapa %s hari ini\n", this, elemenFasilkom);
        }
    }
}
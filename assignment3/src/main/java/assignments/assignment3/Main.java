package assignments.assignment3;

import java.util.Scanner;

public class Main {

    private static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    private static int totalMataKuliah = 0;
    private static int totalElemenFasilkom = 0;

    public static ElemenFasilkom getElemen(String nama) {
        for (ElemenFasilkom elemen: daftarElemenFasilkom) {
            if (elemen.getNama().equals(nama)) return elemen;
        }
        return null;
    }

    public static MataKuliah getMataKuliah(String nama) {
        for (MataKuliah mataKuliah: daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)) return mataKuliah;
        }
        return null;
    }

    public static void addMahasiswa(String nama, long npm) {
        System.out.printf("%s berhasil ditambahkan\n", nama);
        daftarElemenFasilkom[totalElemenFasilkom++] = new Mahasiswa(nama, npm);
    }

    public static void addDosen(String nama) {
        System.out.printf("%s berhasil ditambahkan\n", nama);
        daftarElemenFasilkom[totalElemenFasilkom++] = new Dosen(nama);
    }

    public static void addElemenKantin(String nama) {
        System.out.printf("%s berhasil ditambahkan\n", nama);
        daftarElemenFasilkom[totalElemenFasilkom++] = new ElemenKantin(nama);
    }

    public static void menyapa(String objek1, String objek2) {
        ElemenFasilkom elemen1 = getElemen(objek1);
        ElemenFasilkom elemen2 = getElemen(objek2);

        if (elemen1 == elemen2) {
            System.out.print("[DITOLAK] Objek yang sama tidak bisa saling menyapa\n");
        } else {
            elemen1.menyapa(elemen2);
        }
    }

    public static void addMakanan(String objek, String namaMakanan, long harga) {
        ElemenFasilkom elemen = getElemen(objek);

        if (elemen.getTipe().equals("ElemenKantin")) {
            ((ElemenKantin) elemen).setMakanan(namaMakanan, harga);
        } else {
            System.out.printf("[DITOLAK] %s bukan merupakan elemen kantin\n", elemen);
        }
    }

    public static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        ElemenFasilkom elemen1 = getElemen(objek1);
        ElemenFasilkom elemen2 = getElemen(objek2);

        if (elemen2.getTipe().equals("ElemenKantin")) {
            if (elemen1 == elemen2) {
                System.out.print("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri\n");
            } else {
                elemen1.membeliMakanan(elemen1, elemen2, namaMakanan);
            }
        } else {
            System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
        }
    }

    public static void createMatkul(String nama, int kapasitas) {
        System.out.printf("%s berhasil ditambahkan dengan kapasitas %d\n", nama, kapasitas);
        daftarMataKuliah[totalMataKuliah++] = new MataKuliah(nama, kapasitas);
    }

    public static void addMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom elemen = getElemen(objek);

        if (elemen.getTipe().equals("Mahasiswa")) {
            ((Mahasiswa) elemen).addMatkul(getMataKuliah(namaMataKuliah));
        } else {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
        }
    }

    public static void dropMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom elemen = getElemen(objek);

        if (elemen.getTipe().equals("Mahasiswa")) {
            ((Mahasiswa) elemen).dropMatkul(getMataKuliah(namaMataKuliah));
        } else {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
        }
    }

    public static void mengajarMatkul(String objek, String namaMataKuliah) {
        ElemenFasilkom elemen = getElemen(objek);

        if (elemen.getTipe().equals("Dosen")) {
            ((Dosen) elemen).mengajarMataKuliah(getMataKuliah(namaMataKuliah));
        } else {
            System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
        }
    }

    public static void berhentiMengajar(String objek) {
        ElemenFasilkom elemen = getElemen(objek);

        if (elemen.getTipe().equals("Dosen")) {
            ((Dosen) elemen).dropMataKuliah();
        } else {
            System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
        }
    }

    public static void ringkasanMahasiswa(String objek) {
        ElemenFasilkom elemen = getElemen(objek);

        if (elemen.getTipe().equals("Mahasiswa")) {
            System.out.printf("Nama: %s\n", elemen);
            System.out.printf("Tanggal lahir: %s\n", ((Mahasiswa) elemen).getTanggalLahir());
            System.out.printf("Jurusan: %s\n", ((Mahasiswa) elemen).getJurusan());
            System.out.println("Daftar Mata Kuliah:");
            if (((Mahasiswa) elemen).getJumlahMataKuliah() == 0) {
                System.out.println("Belum ada mata kuliah yang diambil");
            } else {
                int no=1;
                for (MataKuliah mataKuliah: ((Mahasiswa) elemen).getDaftarMataKuliah()) {
                    if (mataKuliah != null) System.out.printf("%d. %s\n", no++, mataKuliah);
                }
            }
        } else {
            System.out.printf("[DITOLAK] %s bukan merupakan seorang mahasiswa\n", elemen);
        }
    }

    public static void ringkasanMataKuliah(String namaMataKuliah) {
        MataKuliah mataKuliah = getMataKuliah(namaMataKuliah);

        System.out.printf("Nama mata kuliah: %s\n", mataKuliah);
        System.out.printf("Jumlah mahasiswa: %s\n", mataKuliah.getJumlahMahasiswa());
        System.out.printf("Kapasitas: %s\n", mataKuliah.getKapasitas());
        System.out.printf("Dosen pengajar: %s\n", mataKuliah.getDosen()==null ? "Belum ada" : mataKuliah.getDosen());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini:");
        if (mataKuliah.getJumlahMahasiswa() == 0) {
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
        } else {
            int no=1;
            for (Mahasiswa mahasiswa: mataKuliah.getDaftarMahasiswa()) {
                if (mahasiswa != null) System.out.printf("%d. %s\n", no++, mahasiswa);
            }
        }
    }

    public static void nextDay() {
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        for (int i=0; i<totalElemenFasilkom; i++) {
            // Nilai friendship bertambah 10 jika menyapa lebih dari sama dengan setengah totalElemenFasilkom selain dirinya
            // Nilai friendship berkurang 5 jika menyapa kurang dari setengah totalElemenFasilkom selain dirinya
            if (daftarElemenFasilkom[i].getJumlahMenyapa() >= Math.ceil(((double) totalElemenFasilkom-1)/2)) {
                daftarElemenFasilkom[i].setFriendship(10);
            } else {
                daftarElemenFasilkom[i].setFriendship(-5);
            }
        }
        friendshipRanking();

        // Mereset jumlah menyapa
        for (int i=0; i<totalElemenFasilkom; i++) {
            daftarElemenFasilkom[i].resetMenyapa();
        }
    }

    public static void friendshipRanking() {
        // Mengurutkan ElemenFasilkom berdasarkan friendship dan nama
        for (int i=0; i<totalElemenFasilkom-1; i++) {
            for (int j=0; j<totalElemenFasilkom-i-1; j++) {
                if (daftarElemenFasilkom[j].getFriendship() < daftarElemenFasilkom[j+1].getFriendship()) {
                    // Menukar daftarElemenFasikom[j] dengan daftarElemenFasikom[j+1]
                    ElemenFasilkom temp = daftarElemenFasilkom[j];
                    daftarElemenFasilkom[j] = daftarElemenFasilkom[j+1];
                    daftarElemenFasilkom[j+1] = temp;
                } else if (daftarElemenFasilkom[j].getFriendship() == daftarElemenFasilkom[j+1].getFriendship() &&
                        daftarElemenFasilkom[j].getNama().compareTo(daftarElemenFasilkom[j+1].getNama())>0) {
                    // Menukar daftarElemenFasikom[j] dengan daftarElemenFasikom[j+1]
                    ElemenFasilkom temp = daftarElemenFasilkom[j];
                    daftarElemenFasilkom[j] = daftarElemenFasilkom[j+1];
                    daftarElemenFasilkom[j+1] = temp;
                }
            }
        }

        // Menampilkan nama dan friendship semua ElemenFasilkom
        for (int i=0; i<totalElemenFasilkom; i++) {
            System.out.printf("%d. %s(%d)\n", (i+1), daftarElemenFasilkom[i], daftarElemenFasilkom[i].getFriendship());
        }
    }

    public static void programEnd() {
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :");
        friendshipRanking();
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
    }
}
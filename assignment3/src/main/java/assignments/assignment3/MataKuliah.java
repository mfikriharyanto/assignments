package assignments.assignment3;

public class MataKuliah {

    private String nama;
    private int kapasitas;
    private Dosen dosen;
    private int jumlahMahasiswa;
    private Mahasiswa[] daftarMahasiswa = new Mahasiswa[100];

    public MataKuliah(String nama, int kapasitas) {
        this.nama = nama;
        this.kapasitas = kapasitas;
    }

    public String getNama() {
        return this.nama;
    }

    public int getKapasitas() {
        return this.kapasitas;
    }

    public Dosen getDosen() {
        return this.dosen;
    }

    public int getJumlahMahasiswa() {
        return this.jumlahMahasiswa;
    }

    public Mahasiswa[] getDaftarMahasiswa() {
        return this.daftarMahasiswa;
    }

    public Mahasiswa getMahasiswa(Mahasiswa mahasiswa) {
        for (Mahasiswa mahasiswaTerdaftar: this.daftarMahasiswa) {
            if (mahasiswaTerdaftar==mahasiswa) return mahasiswaTerdaftar;
        }
        return null;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        for (int i=0; i<this.daftarMahasiswa.length; i++) {
            if (this.daftarMahasiswa[i] == null) {
                this.daftarMahasiswa[i] = mahasiswa;
                this.jumlahMahasiswa++;
                break;
            }
        }
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        for (int i=0; i<this.daftarMahasiswa.length; i++) {
            if (this.daftarMahasiswa[i] == mahasiswa) {
                this.daftarMahasiswa[i] = null;
                this.jumlahMahasiswa--;
                break;
            }
        }
    }

    public void addDosen(Dosen dosen) {
        this.dosen = dosen;
    }

    public void dropDosen() {
        this.dosen = null;
    }

    public String toString() {
        return this.nama;
    }
}
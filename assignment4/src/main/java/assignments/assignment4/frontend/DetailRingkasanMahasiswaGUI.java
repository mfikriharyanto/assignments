package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMahasiswaGUI {
    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Mengatur font
        Font fontGeneral = new Font("SansSerif", Font.PLAIN, 13);
        Font fontBold = new Font("SansSerif", Font.BOLD, 12);

        // Judul Utama
        JLabel titleLabel = new JLabel("Detail Ringkasan Mahasiswa");
        titleLabel.setBorder(new EmptyBorder(10,0,10,0));
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        // Mengatur Label
        JLabel nama = new JLabel("Nama: " + mahasiswa.getNama());
        nama.setFont(fontGeneral);
        JLabel npm = new JLabel("NPM: " + mahasiswa.getNpm());
        npm.setFont(fontGeneral);
        JLabel jurusan = new JLabel("Jurusan: " + mahasiswa.getJurusan());
        jurusan.setFont(fontGeneral);
        JLabel mataKuliah = new JLabel("Daftar Mata Kuliah:");
        mataKuliah.setFont(fontGeneral);
        JLabel sks = new JLabel("Total SKS: " + mahasiswa.getTotalSKS());
        sks.setFont(fontGeneral);
        JLabel irs = new JLabel("Hasil Pengecekan IRS:");
        irs.setFont(fontGeneral);

        // Mengatur Button
        JButton selesai = new JButton("Selesai");
        selesai.setBackground(new Color(156, 198, 52));
        selesai.setForeground(Color.white);
        selesai.setHorizontalAlignment(JButton.CENTER);
        selesai.setPreferredSize(new Dimension(150, 30));

        mahasiswa.cekIRS();

        // Mengatur ukuran panel sesuai jumlah informasi
        int jumlahBaris = 7 + Math.max(1, mahasiswa.getBanyakMatkul()) + Math.max(1, mahasiswa.getBanyakMasalahIRS());
        if (mahasiswa.getBanyakMatkul() >= 6) {
            jumlahBaris = jumlahBaris - mahasiswa.getBanyakMatkul() + 1;
        }
        if (mahasiswa.getBanyakMasalahIRS() >= 6) {
            jumlahBaris = jumlahBaris - mahasiswa.getBanyakMasalahIRS() + 1;
        }
        frame.setSize(500, Math.max(jumlahBaris*25 + 170, 500));
        JPanel detailMahasiswa = new JPanel(new GridLayout(jumlahBaris,1));
        detailMahasiswa.setBackground(Color.WHITE);
        detailMahasiswa.setPreferredSize(new Dimension(350, jumlahBaris*25));

        detailMahasiswa.add(nama);
        detailMahasiswa.add(npm);
        detailMahasiswa.add(jurusan);
        detailMahasiswa.add(mataKuliah);

        if (mahasiswa.getBanyakMatkul() == 0) {
            JLabel temp = new JLabel("Belum ada mata kuliah yang diambil.");
            temp.setFont(fontBold);
            detailMahasiswa.add(temp);
        } else if (mahasiswa.getBanyakMatkul() >= 6) {
            MataKuliah[] matkul = mahasiswa.getMataKuliah();
            String[] daftarMatkul = new String[matkul.length];
            for (int i=0; i<mahasiswa.getBanyakMatkul(); i++) {
                daftarMatkul[i] = (i+1) + ". " + matkul[i].getNama();
            }
            JComboBox<String> namaMatkul = new JComboBox<>(daftarMatkul);
            namaMatkul.setMaximumRowCount(mahasiswa.getBanyakMatkul());
            namaMatkul.setBackground(Color.WHITE);
            detailMahasiswa.add(namaMatkul);
        } else {
            MataKuliah[] matkul = mahasiswa.getMataKuliah();
            for (int i=0; i< mahasiswa.getBanyakMatkul(); i++) {
                JLabel daftarMatkul = new JLabel((i+1) + ". " + matkul[i].getNama());
                daftarMatkul.setFont(fontBold);
                detailMahasiswa.add(daftarMatkul);
            }
        }

        detailMahasiswa.add(sks);
        detailMahasiswa.add(irs);

        if (mahasiswa.getBanyakMasalahIRS() == 0) {
            JLabel temp = new JLabel("IRS tidak bermasalah.");
            temp.setFont(fontBold);
            detailMahasiswa.add(temp);
        } else if (mahasiswa.getBanyakMasalahIRS() >= 6) {
            String[] masalahIRS = mahasiswa.getMasalahIRS();
            for (int i=0; i<mahasiswa.getBanyakMatkul(); i++) {
                masalahIRS[i] = (i+1) + ". " + masalahIRS[i];
            }
            JComboBox<String> status = new JComboBox<>(masalahIRS);
            status.setMaximumRowCount(mahasiswa.getBanyakMasalahIRS());
            status.setBackground(Color.WHITE);
            detailMahasiswa.add(status);
        } else {
            String[] masalahIRS = mahasiswa.getMasalahIRS();
            for (int i=0; i< mahasiswa.getBanyakMasalahIRS(); i++) {
                JLabel status = new JLabel((i+1) + ". " + masalahIRS[i]);
                status.setFont(fontBold);
                detailMahasiswa.add(status);
            }
        }

        // SubPanel
        JPanel container = new JPanel();
        container.setBackground(Color.WHITE);
        container.add(titleLabel);
        container.add(detailMahasiswa);
        container.add(selesai);
        frame.add(container);
        frame.revalidate();

        selesai.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
                frame.remove(container);
                frame.setSize(500, 500);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
    }
}

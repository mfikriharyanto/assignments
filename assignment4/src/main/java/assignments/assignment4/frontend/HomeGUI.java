package assignments.assignment4.frontend;

import javax.swing.JFrame;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HomeGUI {
    
    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Judul Utama
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Selamat datang di Sistem Akademik");
        titleLabel.setBorder(new EmptyBorder(10,0,10,0));
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        // Mengatur SubPanel
        JPanel home = new JPanel();
        home.setBackground(Color.WHITE);
        JPanel gButton = new JPanel(new GridLayout(7,1, 0, 15));
        gButton.setBackground(Color.WHITE);
        gButton.setPreferredSize(new Dimension(300, 350));

        // Mengatur Button
        JButton[] button = new JButton[7];
        String[] text = {"Tambah Mahasiswa", "Tambah Mata Kuliah", "Tambah IRS",
                "Hapus IRS", "Lihat Ringkasan Mahasiswa", "Lihat Ringkasan Mata Kuliah", "Keluar"};
        for (int i=0; i<button.length; i++) {
            button[i] = new JButton(text[i]);
            button[i].setBackground(new Color(156, 198, 52));
            if (i == 6) button[i].setBackground(new Color(220, 90, 100));
            button[i].setForeground(Color.white);
            gButton.add(button[i]);
        }

        home.add(titleLabel);
        home.add(gButton);

        frame.add(home);
        frame.revalidate();

        button[0].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
                frame.remove(home);
                new TambahMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });

        button[1].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
                frame.remove(home);
                new TambahMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });

        button[2].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
                frame.remove(home);
                new TambahIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });

        button[3].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
                frame.remove(home);
                new HapusIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });

        button[4].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
                frame.remove(home);
                new RingkasanMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });

        button[5].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
                frame.remove(home);
                new RingkasanMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });

        button[6].addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
//                System.exit(0);
                frame.dispose();
            }
        });
    }
}

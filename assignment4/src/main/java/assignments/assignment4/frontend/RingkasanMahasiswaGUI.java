package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMahasiswaGUI {

    public RingkasanMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Judul Utama
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Ringkasan Mahasiswa");
        titleLabel.setBorder(new EmptyBorder(10,0,10,0));
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        // Label dan Combobox
        JLabel pilihNPM = new JLabel("Pilih NPM");
        pilihNPM.setHorizontalAlignment(JLabel.CENTER);
        pilihNPM.setBorder(new EmptyBorder(10,0,0,0));
        JComboBox<String> npm = new JComboBox<>(getNPMSorted(daftarMahasiswa));
        npm.setBackground(Color.WHITE);

        // Button
        JButton lihat = new JButton("Lihat");
        lihat.setBackground(new Color(156, 198, 52));
        lihat.setForeground(Color.white);
        JButton kembali = new JButton("Kembali");
        kembali.setBackground(new Color(132, 174, 204));
        kembali.setForeground(Color.white);

        // SubPanel
        JPanel ringkasanMahasiswa = new JPanel(new GridLayout(10,1, 0, 10));
        ringkasanMahasiswa.setBackground(Color.WHITE);
        ringkasanMahasiswa.setPreferredSize(new Dimension(300,360));
        ringkasanMahasiswa.add(pilihNPM);
        ringkasanMahasiswa.add(npm);
        ringkasanMahasiswa.add(lihat);
        ringkasanMahasiswa.add(kembali);

        JPanel container = new JPanel();
        container.setBackground(Color.WHITE);
        container.add(titleLabel);
        container.add(ringkasanMahasiswa);
        frame.add(container);
        frame.revalidate();

        lihat.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
                if (npm.getSelectedItem() == null) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                }  else {
                    String npmTerpilih = npm.getSelectedItem().toString();
                    Mahasiswa mahasiswa = getMahasiswa(Long.parseLong(npmTerpilih), daftarMahasiswa);

                    frame.remove(container);
                    new DetailRingkasanMahasiswaGUI(frame, mahasiswa, daftarMahasiswa, daftarMataKuliah);
                }
            }
        });

        kembali.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
                frame.remove(container);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
    }

    private String[] getNPMSorted (ArrayList<Mahasiswa> daftarMahasiswa) {
        String[] arrNPM = new String[daftarMahasiswa.size()];

        for (int i=0; i<daftarMahasiswa.size(); i++) {
            arrNPM[i] = Long.toString(daftarMahasiswa.get(i).getNpm());
        }

        // Selection Sort
        for (int i = 0; i < arrNPM.length - 1; i++){
            int index = i;
            for (int j = i + 1; j < arrNPM.length; j++){
                if (arrNPM[j].compareTo(arrNPM[index]) < 0) index = j;
            }
            // Swap
            String temp = arrNPM[index];
            arrNPM[index] = arrNPM[i];
            arrNPM[i] = temp;
        }
        return arrNPM;
    }

    private Mahasiswa getMahasiswa(long npm, ArrayList<Mahasiswa> daftarMahasiswa) {
        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
}

package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMataKuliahGUI {

    public RingkasanMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Judul
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Ringkasan Mata Kuliah");
        titleLabel.setBorder(new EmptyBorder(10,0,10,0));
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        // Label dan Combobox
        JLabel pilihNamaMatkul = new JLabel("Pilih Nama Matkul");
        pilihNamaMatkul.setHorizontalAlignment(JLabel.CENTER);
        pilihNamaMatkul.setBorder(new EmptyBorder(10,0,0,0));
        JComboBox<String> namaMatkul = new JComboBox<>(getNamaMatkulSorted(daftarMataKuliah));
        namaMatkul.setBackground(Color.WHITE);

        // Button
        JButton lihat = new JButton("Lihat");
        lihat.setBackground(new Color(156, 198, 52));
        lihat.setForeground(Color.white);
        JButton kembali = new JButton("Kembali");
        kembali.setBackground(new Color(132, 174, 204));
        kembali.setForeground(Color.white);

        // SubPanel
        JPanel ringkasanMataKuliah = new JPanel(new GridLayout(10,1, 0, 10));
        ringkasanMataKuliah.setBackground(Color.WHITE);
        ringkasanMataKuliah.setPreferredSize(new Dimension(300,360));
        ringkasanMataKuliah.add(pilihNamaMatkul);
        ringkasanMataKuliah.add(namaMatkul);
        ringkasanMataKuliah.add(lihat);
        ringkasanMataKuliah.add(kembali);

        JPanel container = new JPanel();
        container.setBackground(Color.WHITE);
        container.add(titleLabel);
        container.add(ringkasanMataKuliah);
        frame.add(container);
        frame.revalidate();

        lihat.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
                if (namaMatkul.getSelectedItem() == null) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                }  else {
                    String namaMatkulTerpilih = namaMatkul.getSelectedItem().toString();
                    MataKuliah mataKuliah= getMataKuliah(namaMatkulTerpilih, daftarMataKuliah);

                    frame.remove(container);
                    new DetailRingkasanMataKuliahGUI(frame, mataKuliah, daftarMahasiswa, daftarMataKuliah);
                }
            }
        });

        kembali.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
                frame.remove(container);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
    }

    private String[] getNamaMatkulSorted (ArrayList<MataKuliah> daftarMataKuliah) {
        String[] arrNamaMatkul = new String[daftarMataKuliah.size()];

        for (int i=0; i<daftarMataKuliah.size(); i++) {
            arrNamaMatkul[i] = daftarMataKuliah.get(i).getNama();
        }

        // Selection Sort
        for (int i = 0; i < arrNamaMatkul.length - 1; i++){
            int index = i;
            for (int j = i + 1; j < arrNamaMatkul.length; j++){
                if (arrNamaMatkul[j].compareTo(arrNamaMatkul[index]) < 0) index = j;
            }
            // Swap
            String temp = arrNamaMatkul[index];
            arrNamaMatkul[index] = arrNamaMatkul[i];
            arrNamaMatkul[i] = temp;
        }
        return arrNamaMatkul;
    }

    private MataKuliah getMataKuliah(String nama, ArrayList<MataKuliah> daftarMataKuliah) {
        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }
}

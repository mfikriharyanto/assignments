package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahIRSGUI {

    public TambahIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Judul Utama
        JLabel titleLabel = new JLabel("      Tambah IRS      ");
        titleLabel.setBorder(new EmptyBorder(10,0,10,0));
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        // Label dan Combobox
        JLabel pilihNPM = new JLabel("Pilih NPM");
        pilihNPM.setHorizontalAlignment(JLabel.CENTER);
        pilihNPM.setBorder(new EmptyBorder(10,0,0,0));
        JComboBox<String> npm = new JComboBox<>(getNPMSorted(daftarMahasiswa));
        npm.setBackground(Color.WHITE);
        JLabel pilihNamaMatkul = new JLabel("Pilih Nama Matkul");
        pilihNamaMatkul.setHorizontalAlignment(JLabel.CENTER);
        pilihNamaMatkul.setBorder(new EmptyBorder(10,0,0,0));
        JComboBox<String> namaMatkul = new JComboBox<>(getNamaMatkulSorted(daftarMataKuliah));
        namaMatkul.setBackground(Color.WHITE);

        // Button
        JButton tambah = new JButton("Tambahkan");
        tambah.setBackground(new Color(156, 198, 52));
        tambah.setForeground(Color.white);
        JButton kembali = new JButton("Kembali");
        kembali.setBackground(new Color(132, 174, 204));
        kembali.setForeground(Color.white);

        // SubPanel
        JPanel tambahIRS = new JPanel(new GridLayout(10,1, 0, 10));
        tambahIRS.setBackground(Color.WHITE);
        tambahIRS.setPreferredSize(new Dimension(300,360));
        tambahIRS.add(pilihNPM);
        tambahIRS.add(npm);
        tambahIRS.add(pilihNamaMatkul);
        tambahIRS.add(namaMatkul);
        tambahIRS.add(tambah);
        tambahIRS.add(kembali);

        JPanel container = new JPanel();
        container.setBackground(Color.WHITE);
        container.add(titleLabel);
        container.add(tambahIRS);
        frame.add(container);
        frame.revalidate();

        tambah.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
                if (npm.getSelectedItem() == null || namaMatkul.getSelectedItem() == null) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                }  else {
                    String npmTerpilih = npm.getSelectedItem().toString();
                    String namaMatkulTerpilih = namaMatkul.getSelectedItem().toString();
                    Mahasiswa mahasiswa = getMahasiswa(Long.parseLong(npmTerpilih), daftarMahasiswa);

                    String status = mahasiswa.addMatkul(getMataKuliah(namaMatkulTerpilih, daftarMataKuliah));
                    JOptionPane.showMessageDialog(frame, status);
                }
            }
        });

        kembali.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
                frame.remove(container);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });

    }

    private String[] getNPMSorted (ArrayList<Mahasiswa> daftarMahasiswa) {
        String[] arrNPM = new String[daftarMahasiswa.size()];

        for (int i=0; i<daftarMahasiswa.size(); i++) {
            arrNPM[i] = Long.toString(daftarMahasiswa.get(i).getNpm());
        }

        // Selection Sort
        for (int i = 0; i < arrNPM.length - 1; i++){
            int index = i;
            for (int j = i + 1; j < arrNPM.length; j++){
                if (arrNPM[j].compareTo(arrNPM[index]) < 0) index = j;
            }
            // Swap
            String temp = arrNPM[index];
            arrNPM[index] = arrNPM[i];
            arrNPM[i] = temp;
        }
        return arrNPM;
    }

    private String[] getNamaMatkulSorted (ArrayList<MataKuliah> daftarMataKuliah) {
        String[] arrNamaMatkul = new String[daftarMataKuliah.size()];

        for (int i=0; i<daftarMataKuliah.size(); i++) {
            arrNamaMatkul[i] = daftarMataKuliah.get(i).getNama();
        }

        // Selection Sort
        for (int i = 0; i < arrNamaMatkul.length - 1; i++){
            int index = i;
            for (int j = i + 1; j < arrNamaMatkul.length; j++){
                if (arrNamaMatkul[j].compareTo(arrNamaMatkul[index]) < 0) index = j;
            }
            // Swap
            String temp = arrNamaMatkul[index];
            arrNamaMatkul[index] = arrNamaMatkul[i];
            arrNamaMatkul[i] = temp;
        }
        return arrNamaMatkul;
    }

    private MataKuliah getMataKuliah(String nama, ArrayList<MataKuliah> daftarMataKuliah) {
        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }

    private Mahasiswa getMahasiswa(long npm, ArrayList<Mahasiswa> daftarMahasiswa) {
        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
}

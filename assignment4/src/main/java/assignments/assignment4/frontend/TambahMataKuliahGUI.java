package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMataKuliahGUI{

    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Judul Utama
        JLabel titleLabel = new JLabel("Tambah Mata Kuliah");
        titleLabel.setBorder(new EmptyBorder(10,0,10,0));
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);

        // Label dan input
        JLabel labelKodeMataKuliah = new JLabel("Kode Mata Kuliah:");
        labelKodeMataKuliah.setHorizontalAlignment(JLabel.CENTER);
        labelKodeMataKuliah.setBorder(new EmptyBorder(10,0,0,0));
        JTextField fieldKodeMatkul = new JTextField();
        JLabel labelNamaMataKuliah = new JLabel("Nama Mata Kuliah:");
        labelNamaMataKuliah.setHorizontalAlignment(JLabel.CENTER);
        labelNamaMataKuliah.setBorder(new EmptyBorder(10,0,0,0));
        JTextField fieldNamaMatkul = new JTextField();
        JLabel labelSKS = new JLabel("SKS:");
        labelSKS.setHorizontalAlignment(JLabel.CENTER);
        labelSKS.setBorder(new EmptyBorder(10,0,0,0));
        JTextField fieldSKS = new JTextField();
        JLabel labelKapasitas = new JLabel("Kapasitas:");
        labelKapasitas.setHorizontalAlignment(JLabel.CENTER);
        labelKapasitas.setBorder(new EmptyBorder(10,0,0,0));
        JTextField fieldKapasitas = new JTextField();

        // Button
        JButton tambah = new JButton("Tambahkan");
        tambah.setBackground(new Color(156, 198, 52));
        tambah.setForeground(Color.white);
        JButton kembali = new JButton("Kembali");
        kembali.setBackground(new Color(132, 174, 204));
        kembali.setForeground(Color.white);

        // SubPanel
        JPanel tambahMataKuliah = new JPanel(new GridLayout(10,1,0,10));
        tambahMataKuliah.setBackground(Color.WHITE);
        tambahMataKuliah.setPreferredSize(new Dimension(300,360));
        tambahMataKuliah.add(labelKodeMataKuliah);
        tambahMataKuliah.add(fieldKodeMatkul);
        tambahMataKuliah.add(labelNamaMataKuliah);
        tambahMataKuliah.add(fieldNamaMatkul);
        tambahMataKuliah.add(labelSKS);
        tambahMataKuliah.add(fieldSKS);
        tambahMataKuliah.add(labelKapasitas);
        tambahMataKuliah.add(fieldKapasitas);
        tambahMataKuliah.add(tambah);
        tambahMataKuliah.add(kembali);

        JPanel container = new JPanel();
        container.setBackground(Color.WHITE);
        container.add(titleLabel);
        container.add(tambahMataKuliah);
        frame.add(container);
        frame.revalidate();

        tambah.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
                String kodeMatkul = fieldKodeMatkul.getText();
                String namaMatkul = fieldNamaMatkul.getText();
                String sks = fieldSKS.getText();
                String kapasitas = fieldKapasitas.getText();
                if (kodeMatkul.isEmpty() || namaMatkul.isEmpty() || sks.isEmpty() || kapasitas.isEmpty()) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                } else if (getMataKuliah(namaMatkul, daftarMataKuliah) != null) {
                    JOptionPane.showMessageDialog(frame, "Mata Kuliah " + namaMatkul + " sudah pernah ditambahkan sebelumnya");
                    fieldKodeMatkul.setText("");
                    fieldNamaMatkul.setText("");
                    fieldSKS.setText("");
                    fieldKapasitas.setText("");
                } else {
                    daftarMataKuliah.add(new MataKuliah(kodeMatkul, namaMatkul, Integer.parseInt(sks), Integer.parseInt(kapasitas)));
                    JOptionPane.showMessageDialog(frame, "Mata Kuliah " + namaMatkul + " berhasil ditambahkan");
                    fieldKodeMatkul.setText("");
                    fieldNamaMatkul.setText("");
                    fieldSKS.setText("");
                    fieldKapasitas.setText("");
                }
            }
        });

        kembali.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
                frame.remove(container);
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
    }

    private MataKuliah getMataKuliah(String nama, ArrayList<MataKuliah> daftarMataKuliah) {
        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }
    
}
